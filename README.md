Quick parser for the Swindon Preservation Society to translate the locking in their XML file to a more human readable format.

Although not explicitly stated in the code, this is released under GPL-2.0 and may be freely copied, altered, distributed etc.