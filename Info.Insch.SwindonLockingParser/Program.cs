﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace Info.Insch.SwindonLockingParser
{
    class Program
    {
        static void Main(string[] args)
        {
            // Hard-coded XML File Name - for testing
            // TODO: Move filename to a command-line parameter
            string inputFile = "Import-Test.xml";

            // Load the XML Document containing Locking Data
            XmlDocument locking = new XmlDocument();
            locking.Load(inputFile);

            // Extract the Locking Conditionals via XPath
            foreach (XmlNode conditional in locking.SelectNodes("/layout-config/conditionals/conditional"))
            {
                // Extract Conditional Attributes
                string systemName = conditional.Attributes["systemName"].InnerText;
                string antecedent = conditional.Attributes["antecedent"].InnerText;

                // Counter for number of conditionals processed, one-based (R1, R2, ..., R99)
                int conditionalCounter = 1;

                // Get the Outer XML for the current conditional to select the inner state variables by XPath
                XmlDocument relays = new XmlDocument();
                relays.LoadXml(conditional.OuterXml.ToString());

                // Loop the Relays (state variables) in the locking conditional
                foreach (XmlNode relay in relays.SelectNodes("/conditional/conditionalStateVariable"))
                {
                    // Extract relay attributes
                    string condition = relay.Attributes["systemName"].InnerText;
                    string state = relay.Attributes["type"].InnerXml;

                    // Detect whether the relay is followed by a space or closing bracket
                    string expressionSpace = String.Format(@"R{0}\s", conditionalCounter.ToString());
                    string expressionBracket = String.Format(@"R{0}\)", conditionalCounter.ToString());
                    string expressionEnd = String.Format(@"R{0}$", conditionalCounter.ToString());
                    bool spaceMatch = Regex.IsMatch(antecedent, expressionSpace);
                    bool bracketMatch = Regex.IsMatch(antecedent, expressionBracket);

                    // Set the suffix to apply in the Regex Replace
                    string replaceSuffix = String.Empty;
                    if ((spaceMatch) && (!bracketMatch))
                    {
                        replaceSuffix = " ";
                    }
                    else if ((!spaceMatch) && (bracketMatch))
                    {
                        replaceSuffix = ")";
                    }

                    // Update the Condition to the value which will be substituted in the Antecedent
                    switch (state)
                    {
                        case "1":
                            condition = String.Format("{0} {1}{2}", condition, "^", replaceSuffix);
                            break;
                        case "2":
                            condition = String.Format("{0} {1}{2}", condition, "v", replaceSuffix);
                            break;
                    }

                    // Perform the Regex Replace
                    if ((spaceMatch) && (!bracketMatch))
                    {
                        antecedent = Regex.Replace(antecedent, expressionSpace, condition);
                    }
                    else if ((!spaceMatch) && (bracketMatch))
                    {
                        antecedent = Regex.Replace(antecedent, expressionBracket, condition);
                    }
                    else
                    {
                        antecedent = Regex.Replace(antecedent, expressionEnd, condition);
                    }

                    // Increment the counter
                    conditionalCounter++;
                }

                // Output the Locking Expression
                Console.WriteLine("{0} = {1}", systemName, antecedent);
            }
        }
    }
}
